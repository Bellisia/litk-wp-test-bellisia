jQuery(document).ready(function($) {
	

	// Trim after 20 characters
	$(".subpages a span").each(function() {
		if($(this).text().length > 20) {
		    $(this).text($(this).text().substring(0, 20) + "...");
		}
	});

	// Sort list based on titles in ascending order
	$(".sort-alphabetically .sort-asc").each(function() {
		$(this).click(function() {
			$(this).parent().siblings().find('>li').sort(function(a, b) {

			return $(a).text() > $(b).text(); 

		}).appendTo($(this).parent().next('.subpages'));
		});
	});

	// Sort list based on titles in descending order
	$(".sort-alphabetically .sort-desc").each(function() {
		$(this).click(function() {
			$(this).parent().siblings().find('>li').sort(function(a, b) {

			return $(a).text() < $(b).text(); 

		}).appendTo($(this).parent().next('.subpages'));
		});
	});


	// Collapse/open items
	$('.collapse-subpages').click(function() {
		$(this).find('i').toggleClass('fa-angle-down fa-angle-up');
		
		$(this).siblings('ul').slideToggle();
	});
     
});