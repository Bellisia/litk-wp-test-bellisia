<?php 
	
	global $post;

	function showSubpages($postId, $currentLevel) {
    
    // Getting the sub pages arguments for the foreach loop so we can get the fitting data
    $subpages = get_posts(array(
        'post_type' =>'page',
        'posts_per_page' =>-1,
        'post_parent' => $postId,
        'order_by' => 'title',
        'order' => 'ASC'
    ));

    if (empty($subpages)) {
    	return;
    }

    $currentLevel = 0;
  

    // Adding some markup for the collapse function and the sorting function, also containing icons from font awesome
    echo '<span class="collapse-subpages"><i class="fa fa-angle-down" aria-hidden="true"></i></span>';

    echo '<div class="sort-alphabetically">';
    echo '<span class="sort sort-asc"><i class="fa fa-long-arrow-up" aria-hidden="true"></i>ASC</span>';
    echo '<span class="sort sort-desc"><i class="fa fa-long-arrow-down" aria-hidden="true"></i>DESC</span>';
    echo '</div>';

    echo '<ul class="subpages level-'.$currentLevel.'-subpages">';

    // Loop through the results we got for our subpages, taking one page at the time
    foreach ($subpages as $page) {

        // Fetching the featured image if there is any
        $image = get_the_post_thumbnail($page->ID, 'thumbnail');

        // Creating the list items for the menu 
        echo '<li>';

        // Getting the permalink
        echo '<a href="'.get_permalink($page->ID).'">';
        
        // If the page has a featured image, we want to print it in our list
        if(isset($image)) {
            echo '<div class="feature_image">' . $image . '</div>';
        }

        // Of course every item has a title, let's get it!
        echo '<span>';
        echo apply_filters('the_title', $page->post_title);
        echo '</span>';

        echo '</a>';

        // Continue the looping through the pages, one step further down in the hierarchy
        showSubpages($page->ID, $currentLevel + 1);

            echo '</li>';

        }

    echo '</ul>';

    }

    // Adding wrapping div so it will work both in main content and in sidebar
    echo '<div class="widget_litk_widget">';

    // Call our function and see the result on the page!
    showSubpages($post->ID, 1);

    echo '</div>';