<?php
/**
* @package LiTK WP Test Bellisia
*/

$newShortcode = new litkWidgetBellisia();
add_shortcode('litk-wp-test-bellisia', array($newShortcode, 'shortcode'));

class litkWidgetBellisia extends WP_Widget {
	
	/**
     * Class constructor
     *
     * Registering the widget
     * Adding id, name and descriotion
     *
     */
	function __construct() {
		parent::__construct(
			'litk_widget',
			__('LiTK WP Test Bellisia', 'text_domain'), 
			array('description' => __('A work test for front end developers', 'text_domain'),) 
		);
	}

	/**
     * The widget
     *
     * Including the frontend of the widget.
     * 
     */

	// Adding the main functionality to the widget 
	public function widget($args, $instance) {

		echo $args['before_widget'];
	
		if (! empty($instance['title'])) {
			echo $args['before_title'] . apply_filters('widget_title', $instance['title']). $args['after_title'];
		}

		// Including function showSubpages containing markup
		include(LITK__PLUGIN_DIR . 'source/templates/widget_frontend.php');

		echo $args['after_widget'];
	}
	
	/**
     * The shortcode
     *
     * Including the frontend in the shortcode.
     * 
     */
	public function shortcode() {

		ob_start();
		require_once(LITK__PLUGIN_DIR . 'source/templates/widget_frontend.php');
		
		$shortcodeFrontend = ob_get_clean();
		return $shortcodeFrontend;
	}

	/**
     * The admin form
     *
     * Adding the admin form for the widget on the widgets page
     * 
     */
	public function form($instance) {
		if (isset($instance[ 'title' ])) {
			$title = $instance[ 'title' ];
		}
		else {
			// Adding a suitable title for the widget only visible on the admin page
			$title = __('LiTK WP Test Bellisia', 'text_domain');
		}

		// Including markup
		include(LITK__PLUGIN_DIR . 'source/templates/widget_admin.php');
	}
	
	/**
     * Update of the admin form (title)
     *
     * Adding update functionality of the title 
     * 
     */
	public function update($newInstance, $oldInstance) {
		$instance = array();
		$instance['title'] = (! empty($newInstance['title'])) ? strip_tags($oldInstance['title']) : '';
		return $instance;
	}
} 