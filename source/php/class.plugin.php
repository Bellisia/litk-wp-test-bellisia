<?php

class litkWpTestBellisia {

    /**
     * Class constructor
     *
     * Loading all SCSS and Scripts
     * 
     * Adding filter for shortcode
     */
    public function __construct() {
        add_action('widgets_init', array($this, 'registerWidgets'));    
        add_action('wp_enqueue_scripts', array($this, 'loadScripts'));
        add_filter( 'widget_text', 'do_shortcode');
    }

    /**
     * Adding all the scripts
     *
     */
    public function loadScripts() {
        wp_enqueue_style('style', LITK__PLUGIN_URL . '/dist/css/style.min.css');
        wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
        wp_enqueue_script('script', LITK__PLUGIN_URL . '/dist/js/packaged.min.js', array( 'jquery'));
    }

    /**
     * Registering the widget litkWidgetBellisia
     *
     */
    public function registerWidgets() {
        register_widget('litkWidgetBellisia');
    }
}

new litkWpTestBellisia();