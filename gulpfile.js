// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
// var imagemin = require('gulp-imagemin');

// Compile Our Sass
gulp.task('sass-dist', function() {
    return gulp.src('source/sass/style.scss')
            .pipe(plumber())
            .pipe(sass())
            .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'))
            .pipe(rename({suffix: '.min'}))
            .pipe(cssnano({
                mergeLonghand: false,
                zindex: false
            }))
            .pipe(gulp.dest('dist/css'))
});

gulp.task('sass-dev', function() {
    return gulp.src('source/sass/style.scss')
            .pipe(plumber())
            .pipe(sass())
            .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'))
            .pipe(rename({suffix: '.dev'}))
            .pipe(gulp.dest('dist/css'))
});

// Concatenate & Minify JS
gulp.task('scripts-dist', function() {
    return gulp.src(['source/js/vendor/*.js', 'source/js/**/*.js', '!source/js/admin/*'])
            .pipe(concat('packaged.js'))
            .pipe(gulp.dest('dist/js'))
            .pipe(rename('packaged.min.js'))
            .pipe(uglify()).on('error', errorHandler)
            .pipe(gulp.dest('dist/js'));
});

gulp.task('scripts-dist-admin', function() {
    return gulp.src(['source/js/admin/*.js'])
            .pipe(concat('admin.js'))
            .pipe(gulp.dest('dist/js'))
            .pipe(rename('admin.min.js'))
            .pipe(uglify()).on('error', errorHandler)
            .pipe(gulp.dest('dist/js'));
});

// Compress images
// gulp.task('compress-images', function () {
//   gulp.src('source/images/**')
//     .pipe(imagemin())
//     .pipe(gulp.dest('dist/images'));
// });

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('source/js/**/*.js', ['scripts-dist']);
    gulp.watch('source/js/admin/*.js', ['scripts-dist-admin']);
    gulp.watch('source/sass/**/*.scss', ['sass-dist', 'sass-dev']);
    // gulp.watch('source/images/**/*', ['compress-images']);
});

// Default Task
gulp.task('default', ['sass-dist', 'sass-dev', 'scripts-dist', 'watch']);

// Handle the error
function errorHandler (error) {
  console.log(error.toString());
  this.emit('end');
}