<?php
/*
Plugin Name: LiTK WP Test Bellisia
Version:     1.0
Author:      Bellisia
*/

// Defining path and url to the plugin
define('LITK__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('LITK__PLUGIN_URL', plugins_url('', __FILE__));

// Including the plugin class and the widget class
require_once(LITK__PLUGIN_DIR . 'source/php/class.plugin.php');
require_once(LITK__PLUGIN_DIR . 'source/php/class.widget.php');